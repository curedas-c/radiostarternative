enum Colors {
  red400 = '#f87171',
  red500 = '#ef4444',
  red600 = '#dc2626',
  red700 = '#b91c1c',
  red800 = '#991b1b',
  darkBlue700 = '#004282',
}

export {Colors};
