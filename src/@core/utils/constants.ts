import {Platform} from 'react-native';
import {Audios} from '../../assets/resource';
import {Emission} from '../models/emission';
import {Replay} from '../models/replay';
import {Rubric} from '../models/rubrics';
import {Video} from '../models/video';

export const STORE_URL =
  Platform.OS === 'android'
    ? 'https://play.google.com/store/apps/details?id=com.whatsapp&hl=fr&gl=US'
    : 'https://apps.apple.com/us/app/subway-surfers/id512939461';

export const FACEBOOK_URL = 'https://www.facebook.com/malibafmbko/';

export const YOUTUBE_URL =
  'https://www.youtube.com/channel/UCc73WtBMqaKIjepoVwaW9SA';

export const TWITTER_URL = 'https://twitter.com/MalibaFm';

export const INSTAGRAM_URL = 'https://www.instagram.com/malian_world/';

export const LINKEDIN_URL =
  'https://www.linkedin.com/in/kadiatou-sy-96218a113/';

export const WHATSAPP_URL = 'whatsapp://send?text=Bonjour&phone=+2250748891994';

export const TELEGRAM_URL = 'https://t.me/TelegramTips/233';

export const REPORT_URL = 'https://mindtech-webdesign.ci';

export const MALIBA_PHONE1 = '00223 20 29 18 66';
export const MALIBA_PHONE2 = '00223 76 45 07 17';
export const MALIBA_PHONE3 = '00223 20 29 14 14';
export const MALIBA_PHONE4 = '00223 97 07 74 74';

export const MALIBA_MAIL = 'malibafmbko@gmail.com';

export const LIVE_TRACK = {
  url: 'https://stream.zeno.fm/q6ypwey6xreuv',
  title: 'Live Radio',
  artist: '99.5',
  artwork: '',
  description: 'Live',
};

export const DEFAULT_EMISSION_LIST: Emission[] = [
  {
    id: '1',
    title: 'Maliba sport long texte',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Sport Divers extra long yeah',
    animators: '',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
  {
    id: '2',
    title: 'Grand Direct',
    logoUrl: '',
    rubric: 'Sport',
    animators: 'Jean Ives',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
  {
    id: '3',
    title: 'Sumu',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Musique',
    animators: 'Granp P',
    description: 'Lorem Ipsum dolor ipset dum. Aseit colum bot calos serei.',
  },
  {
    id: '4',
    title: 'Retro',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Détente',
    animators: '',
    description: '',
  },
];

export const DEFAULT_LATEST_REPLAYS: Replay[] = [
  {
    id: '1',
    title: 'Local Song',
    emissionTitle: 'Tayc',
    logoUrl: 'https://picsum.photos/200',
    streamUrl: Audios.audio1,
  },
  {
    id: '2',
    title: 'Radio.co, la radio choco parmis les radios',
    emissionTitle: 'Jam Fm',
    logoUrl: 'https://picsum.photos/200',
    streamUrl: 'https://listen.radioking.com/radio/61201/stream/98566',
  },
  {
    id: '3',
    title: 'AAC',
    emissionTitle: 'Maliba Fm',
    logoUrl: 'https://picsum.photos/200',
    streamUrl: 'https://stream.zenolive.com/22efuey6xreuv.aac',
  },
  {
    id: '4',
    title: 'MP3 Jango',
    emissionTitle: 'Live song',
    logoUrl: 'https://picsum.photos/200',
    streamUrl: 'https://mp3-128.jango.com/music/04/29/22/0429224990.mp3',
  },
];

export const DEFAULT_RUBRICS: Rubric[] = [
  {
    id: '1',
    name: 'Sport',
  },
  {
    id: '2',
    name: 'Détente',
  },
  {
    id: '3',
    name: 'Musique',
  },
  {
    id: '4',
    name: 'Actualité',
  },
  {
    id: '5',
    name: 'Société',
  },
  {
    id: '6',
    name: 'Divers',
  },
];

export const ALL_EMISSIONS: Emission[] = [
  {
    id: '1',
    title: 'Maliba',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Sport',
    animators: '',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
  {
    id: '2',
    title: 'Grand Direct',
    logoUrl: '',
    rubric: 'Sport',
    animators: 'Jean Ives',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
  {
    id: '3',
    title: 'Sumu',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Musique',
    animators: 'Granp P',
    description: 'Lorem Ipsum dolor ipset dum. Aseit colum bot calos serei.',
  },
  {
    id: '4',
    title: 'Retro',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Détente',
    animators: '',
    description: '',
  },
];

export const SPORT_EMISSIONS: Emission[] = [
  {
    id: '1',
    title: 'Maliba',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Sport',
    animators: '',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
  {
    id: '2',
    title: 'Grand Direct',
    logoUrl: '',
    rubric: 'Sport',
    animators: 'Jean Ives',
    description: 'Lorem Ipsum dolor ipset dum.',
  },
];

export const DETENTE_EMISSIONS: Emission[] = [
  {
    id: '4',
    title: 'Retro',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Détente',
    animators: '',
    description: '',
  },
];
export const MUSIC_EMISSIONS: Emission[] = [
  {
    id: '3',
    title: 'Sumu',
    logoUrl: 'https://picsum.photos/200',
    rubric: 'Musique',
    animators: 'Granp P',
    description: 'Lorem Ipsum dolor ipset dum. Aseit colum bot calos serei.',
  },
];
export const NEWS_EMISSIONS: Emission[] = [];
export const SOCIETY_EMISSIONS: Emission[] = [];
export const OTHERS_EMISSIONS: Emission[] = [];

export const VIDEOS: Video[] = [
  {
    title: 'God of War Intel',
    linkId: '1ySUQqhF6aE',
    date: '12/02/2022',
  },
  {
    title: 'Franglish - Wifey (Clip Officiel)',
    linkId: 'meahILoQ67Q',
    date: '12/02/2022',
  },
  {
    title: 'QKbI5wtwZMY',
    linkId: 'QKbI5wtwZMY',
    date: '12/02/2022',
  },
  {
    title: 'Revolution - Dosez (Clip officiel)',
    linkId: 'pJtkyUub7oo',
    date: '12/02/2022',
  },
  {
    title:
      'Snapdragon 665 - The Legend of Zelda: Twilight Princess - Dolphin emulator [Mi A3]',
    linkId: 'dVCKkj38zAY',
    date: '12/02/2022',
  },
];
