import {Alert, Linking} from 'react-native';
import {WHATSAPP_URL} from './constants';

export const onOpenLink = async (link: string) => {
  await Linking.openURL(link).catch(() => {
    Alert.alert(`Impossible d'effectuer cette action.`);
  });
};

export const onOpenWhatsApp = async () => {
  await Linking.openURL(WHATSAPP_URL).catch(() => {
    Alert.alert('Veuillez installer WhatsApp pour effectuer cette action.');
  });
};
