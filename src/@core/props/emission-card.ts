import {BaseProps} from './base-prop';
import {Emission} from '../models/emission';

export declare type EmissionCardProps = BaseProps & {
  emission: Emission;
  marginRight?: number;
  showTag?: boolean;
};
