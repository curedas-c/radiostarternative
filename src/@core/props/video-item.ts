import {Video} from '../models/video';
import {BaseProps} from './base-prop';

export declare type VideoItemProps = BaseProps & {
  video: Video;
  disableBottomGraySpace: boolean;
};
