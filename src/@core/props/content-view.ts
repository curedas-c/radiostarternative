import {BaseProps} from './base-prop';

export declare type ContentViewProps = BaseProps & {
  backgroundColor?: string;
  barStyle?: 'dark-content' | 'light-content';
  barColor?: string;
  contentPadding?: number;
  showSearchBar?: boolean;
  searchBarConfig?: {
    padding?: number;
    marginBottom?: number;
  };
};
