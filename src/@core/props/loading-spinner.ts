import {BaseProps} from './base-prop';

export declare type LoadingSpinnerProps = BaseProps & {
  fullSize?: boolean;
};
