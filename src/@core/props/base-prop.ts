export declare type BaseProps = {
  children?: React.ReactNode; // best, accepts everything (see edge case below)
  functionChildren?: (name: string) => React.ReactNode; // recommended function as a child render prop type
  style?: React.CSSProperties; // to pass through style props
  onChange?: React.FormEventHandler<HTMLInputElement>;
};
