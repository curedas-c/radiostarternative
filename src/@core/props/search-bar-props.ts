import {BaseProps} from './base-prop';

export declare type SearchBarProps = BaseProps & {
  padding?: number;
  marginBottom?: number;
};
