import {BaseProps} from './base-prop';

export declare type EmissionReplaysProps = BaseProps & {
  emissionId: string;
};
