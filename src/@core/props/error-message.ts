import {BaseProps} from './base-prop';

export declare type ErrorMessageProps = BaseProps & {
  message?: string;
  canRetry?: boolean;
  onRetry?: any;
};
