import {BaseProps} from './base-prop';

export declare type PlayerProps = BaseProps & {
  isLiveRadio?: boolean;
};
