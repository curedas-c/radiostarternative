import {BaseProps} from './base-prop';
import {Replay} from '../models/replay';

export declare type ReplayItemProps = BaseProps & {
  replay: Replay;
  replayIndex: number;
  replayPlaylist: Replay[] | null;
};
