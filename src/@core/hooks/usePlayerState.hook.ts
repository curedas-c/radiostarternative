/* eslint-disable react-hooks/exhaustive-deps */
import {useState, useEffect} from 'react';
import TrackPlayer, {
  useTrackPlayerEvents,
  Event,
  State,
  useProgress,
  Capability,
} from 'react-native-track-player';
import {Track} from '../models/track';
import {playLiveRadio} from '../services/player.service';

// Subscribing to the following events inside MyComponent
const events = [
  Event.PlaybackState,
  Event.PlaybackError,
  Event.PlaybackTrackChanged,
];

export function usePlayerState() {
  const {position, buffered, duration} = useProgress(1000);
  const [isPlaying, setIsPlaying] = useState(false);
  const [isBuffering, setIsBuffering] = useState(false);
  const [isConnecting, setIsConnecting] = useState(false);
  const [haveError, setHaveError] = useState(false);
  const [playingTrack, setTrack] = useState<any>(null);

  useEffect(() => {
    try {
      (async function setup() {
        if (playingTrack !== null) {
          return;
        }

        await TrackPlayer.setupPlayer({
          waitForBuffer: true,
        });

        await TrackPlayer.updateOptions({
          stopWithApp: true,
          alwaysPauseOnInterruption: true,
          capabilities: [
            Capability.Play,
            Capability.Pause,
            Capability.SkipToNext,
            Capability.SkipToPrevious,
            Capability.Stop,
          ],
          compactCapabilities: [
            Capability.Play,
            Capability.Pause,
            Capability.Stop,
          ],
          notificationCapabilities: [
            Capability.Play,
            Capability.Pause,
            Capability.SkipToNext,
            Capability.SkipToPrevious,
            Capability.Stop,
          ],
        });

        await playLiveRadio();
      })();
    } catch (error) {
      console.warn('from usePlayer hook', error);
    }
  }, []);

  useTrackPlayerEvents(events, async event => {
    if (event.type === Event.PlaybackError) {
      setHaveError(true);
    }

    if (event.type === Event.PlaybackState) {
      setHaveError(false);
      setIsBuffering(event.state === State.Buffering);
      setIsConnecting(event.state === State.Connecting);
      setIsPlaying(event.state === State.Playing);
    }

    if (
      event.type === Event.PlaybackTrackChanged &&
      event.nextTrack !== undefined &&
      event.nextTrack !== null
    ) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      setTrack(track);
    }
  });

  return {
    currentTrack: playingTrack as Track,
    isPlaying,
    isBuffering,
    isConnecting,
    haveError,
    duration,
    position,
    buffered,
  } as const;
}
