import TrackPlayer, {RepeatMode, Track} from 'react-native-track-player';
import {Audios} from '../../assets/resource';
import {LIVE_TRACK} from '../utils/constants';

const DEFAULT_TRACKS = [
  {
    url: Audios.audio1,
    title: 'Pure (Demo)',
    artist: 'David Chavez',
    artwork: 'https://i.scdn.co/image/e5c7b168be89098eb686e02152aaee9d3a24e5b6',
  },
];

export const pausePlayer = async () => {
  await TrackPlayer.pause().catch(err => {
    console.log('from player service', err); // ToDO: code jamais executé
  });
};

export const startPlayer = async () => {
  await TrackPlayer.play().catch(err => {
    console.log('from player service', err);
  });
};

export const stopPlayer = async () => {
  await TrackPlayer.stop().catch(err => {
    console.log('from player service', err);
  });
};

export const skipToPreviousTrack = async () => {
  await TrackPlayer.skipToPrevious().catch(err => {
    console.log('from player service', err); // ToDO: code jamais executé
  });
};

export const skipToNextTrack = async () => {
  await TrackPlayer.skipToNext().catch(err => {
    console.log('from player service', err); // ToDO: code jamais executé
  });
};

export const seekTo = async (time: number) => {
  try {
    const currentPosition = await TrackPlayer.getPosition();
    await TrackPlayer.seekTo(currentPosition + time);
  } catch (err) {
    console.log('from player service', err); // ToDO: code jamais executé
  }
};

export const setTrackList = async (
  tracks: Track[],
  trackIndexToPlay: number = 0,
) => {
  if (__DEV__ && tracks?.length === 0) {
    tracks = DEFAULT_TRACKS;
  }

  await TrackPlayer.reset();

  try {
    await TrackPlayer.add(tracks).then(async () => {
      await TrackPlayer.skip(trackIndexToPlay);
      await TrackPlayer.play();
    });
  } catch (error) {
    catchError(error);
  }
};

// export const resetPlayer = async () => {
//   const tracks = await TrackPlayer.getQueue();
//   if (tracks.length > 0) {
//     try {
//       await TrackPlayer.destroy();
//     } catch (error) {
//       catchError(error);
//     }
//   }
// };

const catchError = (err: any) => {
  if (__DEV__) {
    console.log('from player service', err);
  }
};

export const playLiveRadio = async () => {
  await setTrackList([LIVE_TRACK]);
};

export const isRepeatModeActive = async () => {
  const repeatMode = await TrackPlayer.getRepeatMode();
  return repeatMode !== RepeatMode.Off;
};

export const disableRepeatMode = async () => {
  await TrackPlayer.setRepeatMode(RepeatMode.Off);
  return isRepeatModeActive();
};

export const enableRepeatMode = async () => {
  await TrackPlayer.setRepeatMode(RepeatMode.Queue);
  return isRepeatModeActive();
};
