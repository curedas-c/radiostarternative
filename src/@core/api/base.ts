import axios, {ResponseType} from 'axios';

interface RequestOptions {
  url: string;
  params?: any;
  body?: any;
  headers?: any;
  responseType?: ResponseType;
}

const GET = ({url, headers, params, responseType = 'json'}: RequestOptions) =>
  axios.get(url, {
    params,
    headers,
    responseType,
  });

const POST = ({
  url,
  headers,
  params,
  body,
  responseType = 'json',
}: RequestOptions) =>
  axios.post(url, body, {
    params,
    headers,
    responseType,
  });

const PUT = ({
  url,
  headers,
  params,
  body,
  responseType = 'text',
}: RequestOptions) =>
  axios.put(url, body, {
    params,
    headers,
    responseType,
  });

export {GET, POST, PUT};
