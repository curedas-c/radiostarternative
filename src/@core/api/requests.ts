import {GET} from './base';
import {
  ALL_EMISSIONS,
  DETENTE_EMISSIONS,
  MUSIC_EMISSIONS,
  NEWS_EMISSIONS,
  OTHERS_EMISSIONS,
  SOCIETY_EMISSIONS,
  SPORT_EMISSIONS,
} from '../utils/constants';
import {
  DEFAULT_EMISSION_LIST,
  DEFAULT_LATEST_REPLAYS,
} from '../utils/constants';
import {Emission, IEmissionRes} from '../models/emission';
import {AxiosResponse} from 'axios';
import {Rubric, IRubricRes} from '../models/rubrics';
import {IReplayRes, Replay} from '../models/replay';
import {DEFAULT_RUBRICS, VIDEOS} from '../utils/constants';
import {IVideoRes, Video} from '../models/video';
export const BASE_URL = 'https://malibafm.testmindtech.com/api/v1';
export const RES_BASE_URL = 'https://malibafm.testmindtech.com';

const getFeaturedEmissions = () => {
  if (__DEV__) {
    return new Promise<Emission[]>(resolve => {
      setTimeout(() => {
        resolve(DEFAULT_EMISSION_LIST);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/emissions-vedette`,
  }).then((res: AxiosResponse<IEmissionRes[]>) => {
    const response = res.data.map(el => {
      return new Emission(el);
    });
    return response;
  });
};

const getLatestReplays = () => {
  if (__DEV__) {
    return new Promise<Replay[]>(resolve => {
      setTimeout(() => {
        resolve(DEFAULT_LATEST_REPLAYS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/replay`,
  }).then((res: AxiosResponse<IReplayRes[]>) => {
    const response = res.data.map(el => {
      return new Replay(el);
    });
    return response;
  });
};

const getRubricList = () => {
  if (__DEV__) {
    return new Promise<Rubric[]>(resolve => {
      setTimeout(() => {
        resolve(DEFAULT_RUBRICS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/les-rubriques`,
  }).then((res: AxiosResponse<IRubricRes[]>) => {
    const response = res.data.map(el => {
      return new Rubric(el);
    });
    return response;
  });
};

const getAllEmissions = () => {
  if (__DEV__) {
    return new Promise<Emission[]>(resolve => {
      setTimeout(() => {
        resolve(ALL_EMISSIONS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/les-emissions`,
  }).then((res: AxiosResponse<IEmissionRes[]>) => {
    const response = res.data.map(el => {
      return new Emission(el);
    });
    return response;
  });
};

const getEmissionsByTag = (tag: string) => {
  if (__DEV__) {
    return new Promise<Emission[]>(resolve => {
      let response: Emission[] = [];
      switch (tag) {
        case '1':
          response = SPORT_EMISSIONS;
          break;
        case '2':
          response = DETENTE_EMISSIONS;
          break;
        case '3':
          response = MUSIC_EMISSIONS;
          break;
        case '4':
          response = NEWS_EMISSIONS;
          break;
        case '5':
          response = SOCIETY_EMISSIONS;
          break;
        case '6':
          response = OTHERS_EMISSIONS;
          break;
        default:
          response = ALL_EMISSIONS;
          break;
      }
      setTimeout(() => {
        resolve(response);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/emissions/search/${tag}`,
  }).then((res: AxiosResponse<IEmissionRes[]>) => {
    const response = res.data.map(el => {
      return new Emission(el);
    });
    return response;
  });
};

const getEmissionsByTitle = (title: string) => {
  if (__DEV__) {
    return new Promise<Emission[]>(resolve => {
      setTimeout(() => {
        resolve(MUSIC_EMISSIONS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/emissions/search-by-input/${title}`,
  }).then((res: AxiosResponse<IEmissionRes[]>) => {
    const response = res.data.map(el => {
      return new Emission(el);
    });
    return response;
  });
};

const getReplaysByEmissionID = (emissionID: string) => {
  if (__DEV__) {
    return new Promise<Replay[]>(resolve => {
      setTimeout(() => {
        resolve(DEFAULT_LATEST_REPLAYS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/replay/${emissionID}`,
  }).then((res: AxiosResponse<IReplayRes[]>) => {
    const response = res.data.map(el => {
      return new Replay(el);
    });
    return response;
  });
};

const getVideos = () => {
  if (__DEV__) {
    return new Promise<Video[]>(resolve => {
      setTimeout(() => {
        resolve(VIDEOS);
      }, 2500);
    });
  }

  return GET({
    url: `${BASE_URL}/videos`,
  }).then((res: AxiosResponse<IVideoRes[]>) => {
    const response = res.data.map(el => {
      return new Video(el);
    });
    return response;
  });
};

export {
  getFeaturedEmissions,
  getLatestReplays,
  getRubricList,
  getAllEmissions,
  getEmissionsByTag,
  getReplaysByEmissionID,
  getEmissionsByTitle,
  getVideos,
};
