import {RES_BASE_URL} from '../api/requests';
export interface IEmissionRes {
  tid_emission: string;
  cover_emission: string;
  nom_emission: string;
  animateur_emission: string;
  description__value: string;
  tid_rubrique: string;
  nom_rubrique: string;
}

export class Emission {
  id: string;
  title: string;
  logoUrl?: string;
  rubric?: string;
  description?: string;
  animators?: string;
  constructor(res: IEmissionRes) {
    this.id = res.tid_emission;
    this.title = res.nom_emission;
    this.logoUrl = res.cover_emission
      ? `${RES_BASE_URL}${res.cover_emission}`
      : '';
    this.animators = res.animateur_emission || '';
    this.rubric = res.nom_rubrique || '';
    this.description = res.description__value || '';
  }
}
