export interface Track {
  url: string | number; // Load media from the network
  title: string;
  description?: string;
  artist?: string;
  album?: string;
  genre?: string;
  date?: string; // RFC 3339
  artwork?: string; // Load artwork from the network
  duration?: number; // Duration in seconds
}
