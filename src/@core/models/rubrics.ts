export interface IRubricRes {
  tid: string;
  nom_rubrique: string;
}

export class Rubric {
  id: string;
  name: string;

  constructor(res: IRubricRes) {
    this.id = res.tid;
    this.name = res.nom_rubrique;
  }
}
