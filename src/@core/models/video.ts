import getVideoId from 'get-video-id';

export interface IVideoRes {
  titre_video: string;
  url_video: string;
  date_publicatiob: string;
}

export class Video {
  linkId: string;
  title: string;
  date: string;

  constructor(res: IVideoRes) {
    this.title = res.titre_video || '';
    this.linkId = getVideoId(res.url_video).id || '';
    this.date = res.date_publicatiob || '';
  }
}
