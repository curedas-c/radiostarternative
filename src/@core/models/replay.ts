import {RES_BASE_URL} from '../api/requests';
export interface IReplayRes {
  nid_replay: string;
  titre: string;
  nom_emission?: string;
  cover_emission?: string;
  audio?: string;
  description__value?: string;
}

export class Replay {
  id: string;
  title: string;
  emissionTitle: string;
  description?: string;
  logoUrl?: string;
  streamUrl?: string;

  constructor(res: IReplayRes) {
    this.id = res.nid_replay;
    this.title = res.titre;
    this.emissionTitle = res.nom_emission || '';
    this.streamUrl = res.audio ? `${RES_BASE_URL}${res.audio}` : '';
    this.description = res.description__value || '';
    this.logoUrl = res.cover_emission
      ? `${RES_BASE_URL}${res.cover_emission}`
      : '';
  }
}
