import {createContext} from 'react';
import {Track} from '../models/track';

interface IPlayerContext {
  isPlaying: boolean;
  isBuffering: boolean;
  isConnecting: boolean;
  haveError: boolean;
  currentTrack: Track | null;
  duration: number;
  position: number;
  buffered: number;
}

const defaultPlayerState = {
  currentTrack: null,
  isPlaying: false,
  isBuffering: false,
  isConnecting: false,
  haveError: false,
  duration: 0,
  position: 0,
  buffered: 0,
};

export const PlayerContext = createContext<IPlayerContext>(defaultPlayerState);
