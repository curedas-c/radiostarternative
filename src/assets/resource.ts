import {Image, ImageSourcePropType} from 'react-native';
const Images = {
  emissionLogo: uriToNativeUri(require('./images/default_emission_logo.png')),
  map: uriToNativeUri(require('./images/map.png')),
};

const Audios = {
  audio1: require('./sounds/1.aac'),
};

function uriToNativeUri(uri: ImageSourcePropType) {
  return Image.resolveAssetSource(uri).uri;
}

export {Images, Audios};
