import { createSlice } from '@reduxjs/toolkit';


interface AuthState {
    currentUser: any;
}

const initialState: AuthState = {
    currentUser: null,
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCurrentUser(state, action) {
        return {
          ...state,
          currentUser: action.payload,
        }
      }
  }
})

export const { setCurrentUser } = authSlice.actions;
export const authReducer = authSlice.reducer;
