import {createSlice} from '@reduxjs/toolkit';
import {Video} from '../../@core/models/video';
import {fetchVideos} from '../middlewares/video.thunk';
import {RootState} from '../store';

interface VideoState {
  videoLoading: boolean;
  videoLoadingError: string | null;
  videoList: Video[];
}

const initialState: VideoState = {
  videoLoading: false,
  videoLoadingError: null,
  videoList: [],
};

const videoSlice = createSlice({
  name: 'video',
  initialState,
  reducers: {},
  extraReducers: builder => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchVideos.fulfilled, (state, action) => {
      return {
        ...state,
        videoList: action.payload,
        videoLoading: false,
        videoLoadingError: null,
      };
    }),
      builder.addCase(fetchVideos.pending, state => {
        return {
          ...state,
          videoLoading: true,
          videoLoadingError: null,
        };
      }),
      builder.addCase(fetchVideos.rejected, (state, action) => {
        return {
          ...state,
          videoLoading: false,
          videoLoadingError:
            action.error.message || 'Erreur lors du chargement.',
        };
      });
  },
});

export const videoReducer = videoSlice.reducer;
export const selectVideoState = (state: RootState) => state.video;
