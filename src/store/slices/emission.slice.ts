import {createSlice, isAnyOf} from '@reduxjs/toolkit';
import {Emission} from '../../@core/models/emission';
import {Replay} from '../../@core/models/replay';
import {Rubric} from '../../@core/models/rubrics';
import {
  fetchEmissions,
  fetchEmissionsByTag,
  fetchEmissionsByTitle,
  fetchFeaturedEmission,
  fetchLatestReplays,
  fetchReplaysByEmissionID,
  fetchRubrics,
} from '../middlewares/emission.thunk';
import {RootState} from '../store';

interface EmissionState {
  featuredEmissionLoading: boolean;
  featuredEmissionLoadingError: string | null;
  featuredEmission: Emission[] | null;
  latestReplaysLoading: boolean;
  latestReplaysLoadingError: string | null;
  latestReplays: Replay[] | null;
  currentEmission: Emission | null;
  currentEmissionReplays: Replay[] | null;
  currentEmissionReplaysLoading: boolean;
  currentEmissionReplaysLoadingError: string | null;
  currentSearchText: string;
  rubricList: Rubric[] | null;
  rubricsLoading: boolean;
  rubricsError: string | null;
  selectedRubric: Rubric | null;
  emissionsLoading: boolean;
  emissionError: string | null;
  emissions: Emission[];
}

const initialState: EmissionState = {
  featuredEmissionLoading: false,
  featuredEmissionLoadingError: null,
  featuredEmission: null,
  latestReplaysLoading: false,
  latestReplaysLoadingError: null,
  latestReplays: null,
  currentEmission: null,
  currentEmissionReplays: null,
  currentEmissionReplaysLoading: false,
  currentEmissionReplaysLoadingError: null,
  currentSearchText: '',
  rubricList: null,
  rubricsLoading: false,
  rubricsError: null,
  selectedRubric: null,
  emissionsLoading: false,
  emissionError: null,
  emissions: [],
};

const emissionSlice = createSlice({
  name: 'emission',
  initialState,
  reducers: {
    setCurrentEmission(state, action) {
      return {
        ...state,
        currentEmission: action.payload,
        currentEmissionReplays: null,
      };
    },
    setSearchText(state, action) {
      return {
        ...state,
        currentSearchText: action.payload,
      };
    },
    setSelectedRubric(state, action) {
      return {
        ...state,
        selectedRubric: action.payload,
      };
    },
  },
  extraReducers: builder => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchFeaturedEmission.fulfilled, (state, action) => {
      return {
        ...state,
        featuredEmission: action.payload,
        featuredEmissionLoading: false,
        featuredEmissionLoadingError: null,
      };
    }),
      builder.addCase(fetchFeaturedEmission.pending, state => {
        return {
          ...state,
          featuredEmissionLoading: true,
          featuredEmissionLoadingError: null,
          currentEmission: null,
        };
      }),
      builder.addCase(fetchFeaturedEmission.rejected, (state, action) => {
        return {
          ...state,
          featuredEmissionLoading: false,
          featuredEmissionLoadingError:
            action.error.message || 'Erreur lors du chargement.',
        };
      }),
      builder.addCase(fetchLatestReplays.fulfilled, (state, action) => {
        return {
          ...state,
          latestReplays: action.payload,
          latestReplaysLoading: false,
          latestReplaysLoadingError: null,
        };
      }),
      builder.addCase(fetchLatestReplays.pending, state => {
        return {
          ...state,
          latestReplaysLoading: true,
          latestReplaysLoadingError: null,
          currentEmission: null,
        };
      }),
      builder.addCase(fetchLatestReplays.rejected, (state, action) => {
        return {
          ...state,
          latestReplaysLoading: false,
          latestReplaysLoadingError:
            action.error.message || 'Erreur lors du chargement.',
        };
      });
    builder.addCase(fetchReplaysByEmissionID.fulfilled, (state, action) => {
      return {
        ...state,
        currentEmissionReplays: action.payload,
        currentEmissionReplaysLoading: false,
      };
    }),
      builder.addCase(fetchReplaysByEmissionID.pending, state => {
        return {
          ...state,
          currentEmissionReplaysLoading: true,
          currentEmissionReplays: null,
        };
      }),
      builder.addCase(fetchReplaysByEmissionID.rejected, (state, action) => {
        return {
          ...state,
          currentEmissionReplaysLoading: false,
          currentEmissionReplaysLoadingError:
            action.error.message || 'Erreur lors du chargement.',
        };
      }),
      builder.addCase(fetchRubrics.fulfilled, (state, action) => {
        return {
          ...state,
          rubricList: action.payload,
          rubricsLoading: false,
          rubricsError: null,
        };
      }),
      builder.addCase(fetchRubrics.pending, state => {
        return {
          ...state,
          rubricsLoading: true,
          rubricList: null,
          rubricsError: null,
        };
      }),
      builder.addCase(fetchRubrics.rejected, (state, action) => {
        return {
          ...state,
          rubricsLoading: false,
          rubricsError:
            action.error.message ||
            'Impossible de charger les catégories de rubriques.',
        };
      }),
      builder.addMatcher(
        isAnyOf(
          fetchEmissions.fulfilled,
          fetchEmissionsByTitle.fulfilled,
          fetchEmissionsByTag.fulfilled,
        ),
        (state, action) => {
          return {
            ...state,
            emissions: action.payload,
            emissionsLoading: false,
            emissionsError: null,
          };
        },
      ),
      builder.addMatcher(
        isAnyOf(
          fetchEmissions.pending,
          fetchEmissionsByTitle.pending,
          fetchEmissionsByTag.pending,
        ),
        state => {
          return {
            ...state,
            emissionsLoading: true,
            emissionsError: null,
          };
        },
      ),
      builder.addMatcher(
        isAnyOf(
          fetchEmissions.rejected,
          fetchEmissionsByTitle.rejected,
          fetchEmissionsByTag.rejected,
        ),
        (state, action) => {
          return {
            ...state,
            emissionsLoading: false,
            emissionsError: action.error.message || 'Une erreur est survénue.',
          };
        },
      );
  },
});

export const emissionReducer = emissionSlice.reducer;
export const {setCurrentEmission, setSearchText, setSelectedRubric} =
  emissionSlice.actions;
export const selectEmissionState = (state: RootState) => state.emission;
