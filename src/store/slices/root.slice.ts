import {configureStore} from '@reduxjs/toolkit';
import {authReducer} from './auth.slice';
import {emissionReducer} from './emission.slice';
import {videoReducer} from './video.slice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    emission: emissionReducer,
    video: videoReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

// export const store = createStore(rootReducer);
