import { store } from './slices/root.slice';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

// state types
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

// hooks
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
