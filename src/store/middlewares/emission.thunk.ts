import {createAsyncThunk} from '@reduxjs/toolkit';
import {
  getAllEmissions,
  getEmissionsByTag,
  getEmissionsByTitle,
  getFeaturedEmissions,
  getLatestReplays,
  getReplaysByEmissionID,
  getRubricList,
} from '../../@core/api/requests';

const fetchFeaturedEmission = createAsyncThunk(
  'emission/fetchFeaturedEmission',
  async () => {
    return getFeaturedEmissions();
  },
);

const fetchLatestReplays = createAsyncThunk(
  'emission/fetchLatestReplays',
  async () => {
    return getLatestReplays();
  },
);

const fetchReplaysByEmissionID = createAsyncThunk(
  'emission/fetchReplaysByEmissionID',
  async (emissionId: string) => {
    return getReplaysByEmissionID(emissionId);
  },
);

const fetchRubrics = createAsyncThunk('emission/fetchRubrics', async () => {
  return getRubricList();
});

const fetchEmissions = createAsyncThunk('emission/fetchEmissions', async () => {
  return getAllEmissions();
});

const fetchEmissionsByTitle = createAsyncThunk(
  'emission/fetchEmissionsByTitle',
  async (title: string) => {
    return getEmissionsByTitle(title);
  },
);

const fetchEmissionsByTag = createAsyncThunk(
  'emission/fetchEmissionsByTag',
  async (tag: string) => {
    return getEmissionsByTag(tag);
  },
);

export {
  fetchFeaturedEmission,
  fetchLatestReplays,
  fetchReplaysByEmissionID,
  fetchRubrics,
  fetchEmissions,
  fetchEmissionsByTitle,
  fetchEmissionsByTag,
};
