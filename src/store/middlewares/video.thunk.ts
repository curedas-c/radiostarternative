import {createAsyncThunk} from '@reduxjs/toolkit';
import {getVideos} from '../../@core/api/requests';

const fetchVideos = createAsyncThunk('video/fetchVideos', async () => {
  return getVideos();
});

export {fetchVideos};
