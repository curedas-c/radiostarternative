/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Home} from '../views/home';
import {Video} from '../views/video';
import {Replay} from './replay';
import {More} from './more';

import {Flex, Text} from 'native-base';
import {
  RePlayIcon,
  VideoIcon,
  MenuPlusIcon,
  HomeIcon,
  HomeIconActive,
  RePlayIconActive,
  VideoIconActive,
  MenuPlusIconActive,
  RadioIcon,
  RadioIconActive,
} from '../@core/utils/icons';
import {Player} from '../views/player';
import {TouchableOpacity} from 'react-native';
import {PlayerBar} from '../components/player/player-bar';
import {PlayerContext} from '../@core/contexts/player.context';
import {LIVE_TRACK} from '../@core/utils/constants';
import {playLiveRadio} from '../@core/services/player.service';

const Tab = createBottomTabNavigator();

function Tabs() {
  const {currentTrack} = useContext(PlayerContext);
  return (
    <>
      <Tab.Navigator
        tabBar={(props: any) => <TabBar {...props} />}
        screenOptions={() => ({
          headerShown: false,
        })}>
        <Tab.Screen name="Acceuil" component={Home} />
        <Tab.Screen
          name="Replay"
          component={Replay}
          listeners={({navigation}) => ({
            tabPress: e => {
              // Prevent default action
              e.preventDefault();
              navigation.navigate('Replay', {screen: 'ReplayList'});
            },
          })}
        />
        <Tab.Screen
          name="Live"
          component={Player}
          listeners={({navigation}) => ({
            tabPress: e => {
              e.preventDefault();
              if (!currentTrack || currentTrack?.url !== LIVE_TRACK.url) {
                playLiveRadio();
              }
              navigation.navigate('Player');
            },
          })}
        />
        {/* <Tab.Screen name="Live">
          {props => <Player {...props} isLiveRadio={true} />}
        </Tab.Screen> */}
        <Tab.Screen name="Vidéos" component={Video} />
        <Tab.Screen
          name="Plus"
          component={More}
          initialParams={{hidePlayerBar: true}}
        />
      </Tab.Navigator>
    </>
  );
}

const tabBarIcon = ({route, focused}: any) => {
  const TextIcon = (
    <Text
      style={{fontSize: 12}}
      textAlign="center"
      color={focused ? 'red.500' : 'darkBlue.700'}>
      {route.name}
    </Text>
  );

  if (route.name === 'Acceuil') {
    return (
      <Flex justify="center" align="center">
        {focused ? <HomeIconActive size="md" /> : <HomeIcon size="md" />}
        {TextIcon}
      </Flex>
    );
  }
  if (route.name === 'Live') {
    return (
      <Flex justify="center" align="center">
        {focused ? <RadioIconActive size="md" /> : <RadioIcon size="md" />}
        {TextIcon}
      </Flex>
    );
  }
  if (route.name === 'Replay') {
    return (
      <Flex justify="center" align="center">
        {focused ? <RePlayIconActive size="md" /> : <RePlayIcon size="md" />}
        {TextIcon}
      </Flex>
    );
  }
  if (route.name === 'Vidéos') {
    return (
      <Flex justify="center" align="center">
        {focused ? <VideoIconActive size="md" /> : <VideoIcon size="md" />}
        {TextIcon}
      </Flex>
    );
  }
  if (route.name === 'Plus') {
    return (
      <Flex justify="center" align="center">
        {focused ? (
          <MenuPlusIconActive size="md" />
        ) : (
          <MenuPlusIcon size="md" />
        )}
        {TextIcon}
      </Flex>
    );
  }
};

function TabBar({state, descriptors, navigation}: any) {
  const {params} = state.routes[state.index];
  return (
    <Flex borderTopColor="gray.200" borderTopWidth="0.5">
      {!params?.hidePlayerBar && <PlayerBar />}
      <Flex height={16} direction="row" backgroundColor="white">
        {state.routes.map((route: any, index: any) => {
          const {options} = descriptors[route.key];
          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate({name: route.name, merge: true});
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              key={index}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{flex: 1, justifyContent: 'center'}}>
              {tabBarIcon({route, focused: isFocused})}
            </TouchableOpacity>
          );
        })}
      </Flex>
    </Flex>
  );
}

export {Tabs};
