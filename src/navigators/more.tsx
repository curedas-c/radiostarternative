import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {AppInfoList} from '../views/app-info-list';
import {Contacts} from '../components/contacts';
import {About} from '../components/about';

const Stack = createNativeStackNavigator();

function More() {
  return (
    <>
      <Stack.Navigator
        initialRouteName="InfoList"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="InfoList" component={AppInfoList} />
        <Stack.Screen name="Contacts" component={Contacts} />
        <Stack.Screen name="About" component={About} />
      </Stack.Navigator>
    </>
  );
}

export {More};
