import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ReplayList} from '../views/replay-list';
import {EmissionDetails} from '../views/emission-details';

const Stack = createNativeStackNavigator();

function Replay() {
  return (
    <>
      <Stack.Navigator
        initialRouteName="ReplayList"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="ReplayList" component={ReplayList} />
        <Stack.Screen name="EmissionDetails" component={EmissionDetails} />
      </Stack.Navigator>
    </>
  );
}

export {Replay};
