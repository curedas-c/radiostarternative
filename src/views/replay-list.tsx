/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {ContentView} from '../components/content-view';
import {ErrorMessage} from '../components/error/error-message';
import {LoadingSpinner} from '../components/loading-spinner';
import {RubricList} from '../components/rubric-list';
import {EmissionList} from '../components/emission/emission-list';
import {
  fetchEmissions,
  fetchEmissionsByTag,
  fetchEmissionsByTitle,
  fetchRubrics,
} from '../store/middlewares/emission.thunk';
import {selectEmissionState} from '../store/slices/emission.slice';
import {useAppDispatch, useAppSelector} from '../store/store';

function ReplayList() {
  const dispatch = useAppDispatch();
  const {
    rubricList,
    rubricsLoading,
    rubricsError,
    selectedRubric,
    currentSearchText,
  } = useAppSelector(selectEmissionState);

  useEffect(() => {
    dispatch(fetchRubrics());
  }, []);

  useEffect(() => {
    if (currentSearchText !== '') {
      dispatch(fetchEmissionsByTitle(currentSearchText));
    } else if (!selectedRubric) {
      dispatch(fetchEmissions());
    } else {
      dispatch(fetchEmissionsByTag(selectedRubric.id));
    }
  }, [selectedRubric, currentSearchText]);

  const onRetry = () => {
    if (rubricsError) {
      dispatch(fetchRubrics());
    }
  };

  return (
    <>
      <ContentView showSearchBar={true}>
        {rubricsLoading ? <LoadingSpinner fullSize={true} /> : null}
        {rubricsError ? (
          <ErrorMessage canRetry={true} onRetry={onRetry} />
        ) : null}
        {rubricList ? (
          <>
            <RubricList />
            <EmissionList />
          </>
        ) : null}
      </ContentView>
    </>
  );
}

export {ReplayList};
