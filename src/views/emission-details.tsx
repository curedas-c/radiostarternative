import React from 'react';
import {Box, Flex, AspectRatio, Image, VStack, Text} from 'native-base';
import {useAppSelector} from '../store/store';
import {selectEmissionState} from '../store/slices/emission.slice';
import {LoadingSpinner} from '../components/loading-spinner';
import {EmissionReplays} from '../components/replay/emission-replays';
import {Images} from '../assets/resource';
import {ContentView} from '../components/content-view';

function EmissionDetails() {
  const {currentEmission} = useAppSelector(selectEmissionState);

  return (
    <>
      <ContentView showSearchBar={true}>
        {currentEmission ? (
          <Flex
            width="full"
            justify="flex-start"
            direction="column"
            maxHeight="full">
            <Box marginBottom="10">
              <Box paddingX="12">
                <AspectRatio ratio={1 / 1} width="full" marginBottom="3">
                  <Image
                    source={{
                      uri: currentEmission?.logoUrl || Images.emissionLogo,
                    }}
                    fallbackSource={{
                      uri: Images.emissionLogo,
                    }}
                    alt="Logo"
                    width="full"
                    height="full"
                    marginX="auto"
                    rounded="xl"
                    resizeMode={'cover'}
                  />
                </AspectRatio>
              </Box>
              <VStack alignSelf="flex-start" marginTop="4">
                <Text
                  numberOfLines={1}
                  color="black"
                  fontSize="2xl"
                  fontWeight="semibold"
                  textAlign="left"
                  textTransform="capitalize"
                  marginBottom="1">
                  {currentEmission.title || 'Inconnu'}
                </Text>
                {currentEmission?.description ? (
                  <Text
                    numberOfLines={4}
                    color="gray.400"
                    fontSize="md"
                    textAlign="left">
                    {currentEmission?.description}
                  </Text>
                ) : null}
                {currentEmission?.animators ? (
                  <Text
                    numberOfLines={1}
                    color="gray.400"
                    fontSize="md"
                    textAlign="left"
                    marginTop="0.5">
                    Présentation:{' '}
                    <Text color="darkBlue.100">
                      {currentEmission?.animators}
                    </Text>
                  </Text>
                ) : null}
              </VStack>
            </Box>
            <EmissionReplays />
          </Flex>
        ) : (
          <Flex>
            <Box>
              <LoadingSpinner />
            </Box>
          </Flex>
        )}
      </ContentView>
    </>
  );
}

export {EmissionDetails};
