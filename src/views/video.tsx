import React from 'react';
import {ContentView} from '../components/content-view';
import {VideoList} from '../components/video/video-list';

function Video() {
  return (
    <>
      <ContentView showSearchBar={true} contentPadding={4}>
        <VideoList />
      </ContentView>
    </>
  );
}

export {Video};
