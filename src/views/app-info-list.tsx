import React from 'react';
import {
  Flex,
  Pressable,
  VStack,
  Icon as NIcon,
  Text,
  Divider,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {ContentView} from '../components/content-view';
import {ContactIcon, AboutIcon} from '../@core/utils/icons';
import {useNavigation} from '@react-navigation/native';

function AppInfoList() {
  const navigation = useNavigation<any>();
  const onNavigateToContact = () => {
    navigation?.navigate('Contacts');
  };
  const onNavigateToAbout = () => {
    navigation?.navigate('About');
  };

  return (
    <>
      <ContentView showSearchBar={true}>
        <VStack>
          <Pressable onPress={onNavigateToContact}>
            <Flex
              paddingY="2"
              justify="flex-start"
              direction="row"
              align="center">
              <ContactIcon size="lg" />
              <Text
                marginLeft="4"
                numberOfLines={1}
                color="darkBlue.700"
                fontSize="lg"
                fontWeight="extrabold"
                textAlign="left"
                textTransform="capitalize">
                Contacts
              </Text>
              <NIcon
                as={Icon}
                name="chevron-forward"
                size="md"
                color="darkBlue.700"
                marginLeft="auto"
              />
            </Flex>
          </Pressable>
          <Divider my="2" />
          <Pressable onPress={onNavigateToAbout}>
            <Flex
              paddingY="2"
              justify="flex-start"
              direction="row"
              align="center"
              marginBottom="5">
              <AboutIcon size="lg" />
              <Text
                marginLeft="4"
                numberOfLines={1}
                color="darkBlue.700"
                fontSize="lg"
                fontWeight="extrabold"
                textAlign="left"
                textTransform="capitalize">
                à propos
              </Text>
              <NIcon
                as={Icon}
                name="chevron-forward"
                size="md"
                color="darkBlue.700"
                marginLeft="auto"
              />
            </Flex>
          </Pressable>
        </VStack>
      </ContentView>
    </>
  );
}

export {AppInfoList};
