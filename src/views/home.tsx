import React, {useEffect} from 'react';
import {Box} from 'native-base';
import {FeaturedEmissions} from '../components/emission/featured-emissions';
import {LatestReplay} from '../components/replay/latest-replay';
import {ContentView} from '../components/content-view';
import {useAppDispatch, useAppSelector} from '../store/store';
import {
  fetchFeaturedEmission,
  fetchLatestReplays,
} from '../store/middlewares/emission.thunk';
import {selectEmissionState} from '../store/slices/emission.slice';
import {LoadingSpinner} from '../components/loading-spinner';
import {ErrorMessage} from '../components/error/error-message';

function Home() {
  const dispatch = useAppDispatch();
  const {
    latestReplaysLoading,
    latestReplaysLoadingError,
    featuredEmissionLoading,
    featuredEmissionLoadingError,
    featuredEmission,
    latestReplays,
  } = useAppSelector(selectEmissionState);

  useEffect(() => {
    dispatch(fetchFeaturedEmission());
    dispatch(fetchLatestReplays());
  }, [dispatch]);

  const onRetry = () => {
    if (featuredEmissionLoadingError) {
      dispatch(fetchFeaturedEmission());
    }
    if (latestReplaysLoadingError) {
      dispatch(fetchLatestReplays());
    }
  };
  return (
    <>
      <ContentView showSearchBar={true}>
        {(latestReplaysLoading || featuredEmissionLoading) && (
          <LoadingSpinner />
        )}
        {(latestReplaysLoadingError || featuredEmissionLoadingError) && (
          <ErrorMessage canRetry={true} onRetry={onRetry} />
        )}
        {featuredEmission && latestReplays && (
          <>
            <FeaturedEmissions />
            <Box marginTop="8">
              <LatestReplay />
            </Box>
          </>
        )}
      </ContentView>
    </>
  );
}

export {Home};
