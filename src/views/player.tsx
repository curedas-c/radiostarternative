/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import {
  Flex,
  VStack,
  Text,
  Image,
  StatusBar,
  AspectRatio,
  Box,
} from 'native-base';
import {useContext} from 'react';
import {TrackControls} from '../components/player/track-controls';
import {TrackActions} from '../components/player/track-actions';
import {PlayerContext} from '../@core/contexts/player.context';
import {SafeAreaView} from 'react-native';
import {TrackTime} from '../components/player/track-time';
import {Images} from '../assets/resource';

function Player() {
  const {currentTrack} = useContext(PlayerContext);

  return (
    <>
      <SafeAreaView>
        <StatusBar barStyle="dark-content" />
        <Flex
          backgroundColor="white"
          justify="flex-start"
          align="center"
          height="100%"
          paddingTop="4"
          paddingBottom="2">
          <Box paddingX="12">
            <AspectRatio ratio={1 / 1} width="full">
              <Image
                source={{
                  uri: currentTrack?.artwork || Images.emissionLogo,
                }}
                fallbackSource={{
                  uri: Images.emissionLogo,
                }}
                alt="Logo"
                width="full"
                height="full"
                rounded="xl"
                resizeMode={'cover'}
              />
            </AspectRatio>
          </Box>
          <VStack
            alignSelf="flex-start"
            marginTop="5"
            marginBottom="3"
            paddingX="4">
            <Text
              numberOfLines={1}
              color="black"
              fontSize="xl"
              fontWeight="extrabold"
              textAlign="left"
              textTransform="capitalize"
              marginBottom="1">
              {currentTrack?.title || 'Live Radio'}
            </Text>
            {currentTrack?.artist && (
              <Text
                numberOfLines={2}
                color="darkBlue.100"
                fontSize="md"
                fontWeight="bold"
                textAlign="left">
                {currentTrack?.artist}
              </Text>
            )}
          </VStack>
          <Flex marginTop="2" paddingX="2" justify="center" width="full">
            <TrackTime />
          </Flex>
          <TrackControls />
          <Box marginTop="auto" marginX="auto" paddingX="2.5">
            <TrackActions />
          </Box>
        </Flex>
      </SafeAreaView>
    </>
  );
}

export {Player};
