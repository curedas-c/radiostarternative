/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {ScrollView, Button} from 'native-base';
import {LogBox} from 'react-native';
import {Rubric} from '../@core/models/rubrics';
import {useAppDispatch, useAppSelector} from '../store/store';
import {
  setSearchText,
  selectEmissionState,
  setSelectedRubric,
} from '../store/slices/emission.slice';

function RubricList() {
  const dispatch = useAppDispatch();
  const {rubricList, selectedRubric} = useAppSelector(selectEmissionState);
  const [visibleRubrics, setVisibleRubrics] = useState<any>(null);
  const defaultButton = (
    <Button
      marginRight="2"
      colorScheme="red"
      key={0}
      variant={!selectedRubric ? 'subtle' : 'outline'}
      onPress={() => updateSelectedRubric(null)}>
      Tout
    </Button>
  );

  useEffect(() => {
    LogBox.ignoreLogs(['NativeBase:']);
    renderRubricList();
  }, [selectedRubric, rubricList]);

  const renderRubricList = () => {
    const buttonRubrics = rubricList?.map((rubric, key) => {
      return (
        <Button
          marginRight="2"
          colorScheme="red"
          key={key + 1}
          variant={
            selectedRubric && selectedRubric?.id === rubric.id
              ? 'subtle'
              : 'outline'
          }
          onPress={() => updateSelectedRubric(rubric)}>
          {rubric.name}
        </Button>
      );
    });

    setVisibleRubrics([defaultButton, ...buttonRubrics]);
  };

  const updateSelectedRubric = (rubric: Rubric | null) => {
    if (selectedRubric && selectedRubric?.id === rubric?.id) {
      return;
    }
    dispatch(setSearchText(''));
    dispatch(setSelectedRubric(rubric));
  };

  return (
    <>
      {visibleRubrics ? (
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            display: 'flex',
            flexDirection: 'row',
          }}>
          {visibleRubrics}
        </ScrollView>
      ) : null}
    </>
  );
}

export {RubricList};
