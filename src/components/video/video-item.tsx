import React, {useCallback, useState} from 'react';
import {Box, Center, Flex, Skeleton, Text, VStack} from 'native-base';
import YoutubePlayer from 'react-native-youtube-iframe';
import {VideoItemProps} from '../../@core/props/video-item';

function VideoItem({video}: VideoItemProps) {
  const [isVisible, setIsVisible] = useState(false);
  const [haveError, setHaveError] = useState(false);
  const [playing, setPlaying] = useState(false);

  const onStateChange = useCallback(state => {
    if (state === 'ended') {
      setPlaying(false);
    }
  }, []);

  const onReady = () => {
    setIsVisible(true);
  };

  const onError = () => {
    setHaveError(true);
  };

  return (
    <>
      {video?.linkId ? (
        <Flex position="relative">
          <YoutubePlayer
            height={200}
            play={playing}
            videoId={video.linkId}
            onChangeState={onStateChange}
            onError={onError}
            onReady={onReady}
            initialPlayerParams={{
              controls: false,
              modestbranding: true,
              iv_load_policy: 3,
            }}
          />
          {video?.title ? (
            <Box
              backgroundColor="gray.50"
              paddingX="2"
              paddingTop="1.5"
              paddingBottom="2.5">
              <Text
                numberOfLines={2}
                color="black"
                fontSize="md"
                textAlign="left">
                {video.title}
              </Text>
            </Box>
          ) : null}
          {!isVisible && !haveError ? (
            <Center
              w="100%"
              position="absolute"
              top="0"
              left="0"
              bottom="0"
              backgroundColor="white">
              <VStack w="100%" space={1} overflow="hidden" position="relative">
                <Skeleton h={200} startColor="red.100" />
                <Skeleton.Text
                  px="5"
                  fontSize="sm"
                  lines={2}
                  fadeDuration={3000}
                />
              </VStack>
            </Center>
          ) : null}
        </Flex>
      ) : null}
    </>
  );
}

export {VideoItem};
