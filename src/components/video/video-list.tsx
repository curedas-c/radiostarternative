/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {FlatList} from 'native-base';
import {VideoItem} from './video-item';
import {LoadingSpinner} from '../loading-spinner';
import {useAppDispatch, useAppSelector} from '../../store/store';
import {selectVideoState} from '../../store/slices/video.slice';
import {fetchVideos} from '../../store/middlewares/video.thunk';
import {ErrorMessage} from '../error/error-message';

function VideoList() {
  const dispatch = useAppDispatch();
  const {videoList, videoLoading, videoLoadingError} =
    useAppSelector(selectVideoState);
  const cardItem = ({item, index}: any) => {
    const isLastItem = index === videoList?.length - 1;
    return <VideoItem video={item} disableBottomGraySpace={isLastItem} />;
  };

  useEffect(() => {
    dispatch(fetchVideos());
  }, []);

  const onRetry = () => {
    dispatch(fetchVideos());
  };

  return (
    <>
      <FlatList
        data={videoList}
        renderItem={cardItem}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.linkId}
      />
      {videoLoading && <LoadingSpinner fullSize={true} />}
      {videoLoadingError && (
        <ErrorMessage message={videoLoadingError} onRetry={onRetry} />
      )}
    </>
  );
}

export {VideoList};
