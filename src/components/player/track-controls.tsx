import React, {useContext} from 'react';
import {Flex, Icon as NIcon, Pressable, Spinner} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {PlayerContext} from '../../@core/contexts/player.context';
import {
  pausePlayer,
  skipToNextTrack,
  skipToPreviousTrack,
  startPlayer,
} from '../../@core/services/player.service';

function TrackControls() {
  const {isPlaying, isBuffering, isConnecting, haveError} =
    useContext(PlayerContext);

  const onClickPrev = () => {
    skipToPreviousTrack();
  };
  const onClickNext = () => {
    skipToNextTrack();
  };
  const onClickPlay = () => {
    isPlaying ? pausePlayer() : startPlayer();
  };
  const onRestart = () => {
    startPlayer();
  };
  return (
    <>
      <Flex width="4/5" justify="space-evenly" align="center" direction="row">
        {isBuffering || isConnecting ? (
          <Spinner
            accessibilityLabel="Chargement"
            color="gray.400"
            size="lg"
            marginTop="2"
          />
        ) : null}

        {!isBuffering && !isConnecting ? (
          <>
            <Pressable
              onPress={onClickPrev}
              _pressed={{
                style: {
                  transform: [
                    {
                      scale: 0.92,
                    },
                  ],
                },
              }}>
              <NIcon
                as={Icon}
                name="play-skip-back"
                size="md"
                color="red.700"
              />
            </Pressable>
            {!haveError ? (
              <Pressable
                onPress={onClickPlay}
                _pressed={{
                  style: {
                    transform: [
                      {
                        scale: 0.96,
                      },
                    ],
                  },
                }}>
                <NIcon
                  as={Icon}
                  name={isPlaying ? 'pause-circle' : 'play-circle'}
                  size="3xl"
                  color="red.500"
                />
              </Pressable>
            ) : null}
            {haveError ? (
              <Pressable
                onPress={onRestart}
                padding="1"
                _pressed={{
                  style: {
                    transform: [
                      {
                        scale: 0.96,
                      },
                    ],
                  },
                }}>
                <NIcon
                  as={Icon}
                  name="reload-circle"
                  size="3xl"
                  color="red.700"
                />
              </Pressable>
            ) : null}
            <Pressable
              onPress={onClickNext}
              _pressed={{
                style: {
                  transform: [
                    {
                      scale: 0.92,
                    },
                  ],
                },
              }}>
              <NIcon
                as={Icon}
                name="play-skip-forward"
                size="md"
                color="red.700"
              />
            </Pressable>
          </>
        ) : null}
      </Flex>
    </>
  );
}

export {TrackControls};
