import React, {useContext, useEffect, useState} from 'react';
import {Flex, Slider, Text} from 'native-base';
import {PlayerContext} from '../../@core/contexts/player.context';
import {timeFormat} from '../../@core/utils/track-transform';

function TrackTime() {
  const {duration = null, position} = useContext(PlayerContext);
  const [sliderPosition, setSliderPosition] = useState(0);
  useEffect(() => {
    if (duration) {
      setSliderPosition((position / duration) * 100);
    }
  }, [duration, position]);

  return (
    <>
      <Flex w="full" justify="center" align="center">
        <Slider
          width="90%"
          marginX="auto"
          defaultValue={0}
          value={sliderPosition}
          isReadOnly={true}
          colorScheme="red">
          <Slider.Track>
            <Slider.FilledTrack />
          </Slider.Track>
          <Slider.Thumb />
        </Slider>
        <Flex width="90%" justify="space-between" direction="row">
          <Text color="gray.400" fontSize="xs" textTransform="capitalize">
            {timeFormat(position)}
          </Text>
          <Text color="gray.400" fontSize="xs" textTransform="capitalize">
            {timeFormat(duration ? duration - position : null)}
          </Text>
        </Flex>
      </Flex>
    </>
  );
}

export {TrackTime};
