import React, {useContext, useState, useEffect} from 'react';
import {Flex, Icon as NIcon, Pressable} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {NavigationContext} from '@react-navigation/native';
import {PlayerContext} from '../../@core/contexts/player.context';
import {
  disableRepeatMode,
  enableRepeatMode,
} from '../../@core/services/player.service';
import {
  stopPlayer,
  isRepeatModeActive,
} from '../../@core/services/player.service';

function TrackActions() {
  const [repeat, setRepeat] = useState<boolean>(false);
  const {isPlaying} = useContext(PlayerContext);
  const navigation = useContext(NavigationContext);

  useEffect(() => {
    (async function setRepeatMode() {
      const repeatActive = await isRepeatModeActive();
      setRepeat(repeatActive);
    })();
  }, []);

  const onGoBack = () => {
    navigation?.canGoBack()
      ? navigation?.goBack()
      : navigation?.navigate('Tabs');
  };

  const onClickStop = () => {
    if (isPlaying) {
      stopPlayer();
    }
  };

  const onClickRepeat = async () => {
    if (repeat) {
      const newRepeatMode = await disableRepeatMode();
      setRepeat(newRepeatMode);
    } else {
      const newRepeatMode = await enableRepeatMode();
      setRepeat(newRepeatMode);
    }
  };
  return (
    <>
      <Flex w="full" justify="space-between" direction="row">
        <Pressable onPress={onClickRepeat} paddingY="2" paddingX="4">
          <NIcon
            as={Icon}
            name="repeat"
            size="sm"
            color={repeat ? 'red.700' : 'warmGray.400'}
          />
        </Pressable>

        <Pressable
          onPress={onClickStop}
          paddingY="2"
          paddingX="4"
          _pressed={{
            style: {
              transform: [
                {
                  scale: 0.92,
                },
              ],
            },
          }}>
          <NIcon as={Icon} name="stop" size="sm" color="red.700" />
        </Pressable>

        <Pressable onPress={onGoBack} paddingY="2" paddingX="4">
          <NIcon as={Icon} name="chevron-down" size="sm" color="warmGray.500" />
        </Pressable>
      </Flex>
    </>
  );
}

export {TrackActions};
