import React from 'react';
import {PlayerContext} from '../../@core/contexts/player.context';
import {usePlayerState} from '../../@core/hooks/usePlayerState.hook';
import {BaseProps} from '../../@core/props/base-prop';

function PlayerProvider({children}: BaseProps) {
  const {
    isPlaying,
    isBuffering,
    isConnecting,
    haveError,
    currentTrack,
    duration,
    position,
    buffered,
  } = usePlayerState();
  return (
    <>
      <PlayerContext.Provider
        value={{
          isPlaying,
          isBuffering,
          isConnecting,
          haveError,
          currentTrack,
          duration,
          position,
          buffered,
        }}>
        {children}
      </PlayerContext.Provider>
    </>
  );
}

export {PlayerProvider};
