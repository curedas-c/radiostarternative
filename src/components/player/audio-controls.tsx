import React from 'react';
import {Flex, Icon as NIcon, Slider} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {useState} from 'react';

function AudioControls() {
  const [audioVolume, setAudioVolume] = useState(50);

  return (
    <>
      <Flex w="full" justify="center" align="center" direction="row">
        <NIcon
          as={Icon}
          name="volume-off-outline"
          size="xs"
          color="warmGray.500"
        />
        <Slider
          defaultValue={audioVolume}
          colorScheme="red"
          width="80%"
          marginX="1"
          onChange={v => {
            setAudioVolume(Math.floor(v));
          }}>
          <Slider.Track>
            <Slider.FilledTrack />
          </Slider.Track>
          <Slider.Thumb />
        </Slider>
        <NIcon as={Icon} name="volume-high" size="xs" color="warmGray.500" />
      </Flex>
    </>
  );
}

export {AudioControls};
