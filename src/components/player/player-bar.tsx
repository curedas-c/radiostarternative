/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import {
  Flex,
  Text,
  Image,
  Icon as NIcon,
  Pressable,
  Spinner,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {Images} from '../../assets/resource';
import {useContext} from 'react';
import {NavigationContext} from '@react-navigation/native';
import {pausePlayer, startPlayer} from '../../@core/services/player.service';
import {PlayerContext} from '../../@core/contexts/player.context';

function PlayerBar() {
  const {isPlaying, isBuffering, isConnecting, haveError, currentTrack} =
    useContext(PlayerContext);
  const navigation = useContext(NavigationContext);

  const onClickStart = () => {
    isPlaying ? pausePlayer() : startPlayer();
  };

  const onRestart = () => {
    startPlayer();
  };

  const onClickPlayer = () => {
    currentTrack && navigation?.navigate('Player');
  };

  return (
    <>
      {currentTrack && (
        <Pressable onPress={onClickPlayer}>
          <Flex
            height={16}
            direction="row"
            justify="space-between"
            align="center"
            paddingY="1"
            paddingLeft="3.5"
            paddingRight="0"
            backgroundColor="warmGray.100"
            borderTopRightRadius="3xl"
            borderTopLeftRadius="3xl">
            <Flex
              direction="row"
              justify="flex-start"
              align="center"
              shrink="1">
              <Image
                source={{
                  uri: currentTrack?.artwork || Images.emissionLogo,
                }}
                fallbackSource={{
                  uri: Images.emissionLogo,
                }}
                alt="Logo"
                size="xs"
                resizeMode={'contain'}
                borderRadius={100}
              />
              <Text
                numberOfLines={1}
                flexShrink="1"
                marginLeft="4"
                color="gray.600"
                fontSize="md"
                fontWeight="semibold"
                textAlign="left">
                {currentTrack?.title || currentTrack?.artist || ''}
              </Text>
            </Flex>
            {(isBuffering || isConnecting) && !haveError ? (
              <Spinner
                accessibilityLabel="Chargement"
                color="gray.400"
                size="lg"
                padding="2"
              />
            ) : null}
            {!isBuffering && !isConnecting && !haveError ? (
              <Pressable
                onPress={onClickStart}
                padding="1"
                _pressed={{
                  style: {
                    transform: [
                      {
                        scale: 0.96,
                      },
                    ],
                  },
                }}>
                <NIcon
                  as={Icon}
                  name={isPlaying ? 'pause-circle' : 'play-circle'}
                  size="12"
                  color="red.700"
                />
              </Pressable>
            ) : null}
            {haveError ? (
              <Pressable
                onPress={onRestart}
                padding="1"
                _pressed={{
                  style: {
                    transform: [
                      {
                        scale: 0.96,
                      },
                    ],
                  },
                }}>
                <NIcon
                  as={Icon}
                  name="reload-circle"
                  size="12"
                  color="red.700"
                />
              </Pressable>
            ) : null}
          </Flex>
        </Pressable>
      )}
    </>
  );
}

export {PlayerBar};
