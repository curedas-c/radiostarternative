import React from 'react';
import {Flex, Text} from 'native-base';
import VersionNumber from 'react-native-version-number';
import {ContentView} from './content-view';
import {Alert, Linking, Share} from 'react-native';
import {REPORT_URL, STORE_URL} from '../@core/utils/constants';

function About() {
  const appVersion = VersionNumber.appVersion || '1.0.0';

  const onShareApp = async () => {
    try {
      await Share.share({
        title: `Partager L'application`,
        url: STORE_URL,
        message: STORE_URL,
      });
    } catch (error) {}
  };

  const onReportBug = async () => {
    await Linking.openURL(REPORT_URL).catch(() => {
      Alert.alert(`Impossible d'effectuer cette action`);
    });
  };

  return (
    <>
      <ContentView showSearchBar={true}>
        <Flex height="full" align="center" justify="space-between">
          <Flex>
            <Text
              color="darkBlue.100"
              fontSize="md"
              textAlign="left"
              paddingBottom="1.5">
              Créée en mars 2012, <Text fontWeight="semibold">Maliba FM</Text>,
              sise à Hamdallaye ACI 2000-Bamako, est une radio commerciale mise
              sur les fonts baptismaux par des professionnels aguerris du milieu
            </Text>
            <Text
              color="darkBlue.100"
              fontSize="md"
              textAlign="left"
              paddingBottom="1.5">
              Après près de 9 ans de gestion,
              <Text fontWeight="semibold">
                Bandjougou Tounkara « Djougouss»
              </Text>
              , Promoteur et Président Directeur Général, a transmis le
              flambeau, fin 2021, à
              <Text fontWeight="semibold"> Baba Cissouma</Text> pour donner une
              nouvelle trajectoire à la radio conforme à l’évolution du temps.
            </Text>
            <Text color="darkBlue.100" fontSize="md" textAlign="left">
              Dans son programme de diffusion diversifié (Sport, Santé,
              Education, Culture, Divertissement, etc.), en plus des sujets
              locaux et internationaux, Maliba FM prend en compte également la
              diaspora malienne pour réellement constituer{' '}
              <Text fontWeight="semibold">La Voix du Peuple</Text> qui est son
              slogan.
            </Text>
          </Flex>
          <Flex width="full" align="center" justify="center">
            <Flex width="full" justify="space-between" direction="row">
              <Text
                onPress={onShareApp}
                paddingY="2"
                color="darkBlue.600"
                fontSize="sm"
                fontWeight="extrabold"
                textDecorationLine="underline">
                Partager l'application
              </Text>
              <Text
                onPress={onReportBug}
                paddingY="2"
                color="darkBlue.600"
                fontSize="sm"
                fontWeight="extrabold"
                textAlign="right"
                textDecorationLine="underline">
                Signaler un problème
              </Text>
            </Flex>
            <Text
              numberOfLines={1}
              marginTop="2"
              color="gray.400"
              fontSize="sm"
              fontWeight="light"
              textAlign="center">
              Version {appVersion}
            </Text>
          </Flex>
        </Flex>
      </ContentView>
    </>
  );
}

export {About};
