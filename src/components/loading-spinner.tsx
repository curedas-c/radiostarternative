import React from 'react';
import {Flex, Spinner} from 'native-base';
import {LoadingSpinnerProps} from '../@core/props/loading-spinner';

function LoadingSpinner({fullSize = false}: LoadingSpinnerProps) {
  return (
    <>
      <Flex
        align="center"
        justify="center"
        direction="row"
        height={fullSize ? 'full' : 'auto'}>
        <Spinner accessibilityLabel="Chargement" color="red.700" size="lg" />
        {/* <Heading color="red.700" fontSize="md">
          Chargement...
        </Heading> */}
      </Flex>
    </>
  );
}

export {LoadingSpinner};
