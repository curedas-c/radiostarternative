/* eslint-disable quotes */
import React from 'react';
import {Text, Flex, Button} from 'native-base';
import {ErrorMessageProps} from '../../@core/props/error-message';

function ErrorMessage({message, canRetry, onRetry}: ErrorMessageProps) {
  return (
    <>
      <Flex justify="center" align="center">
        <Text color="black" fontSize="md" textAlign="center">
          {message || `Erreur lors du chargement des données.`}
        </Text>
        {canRetry && (
          <Button
            size="sm"
            variant="subtle"
            colorScheme="red"
            marginTop="2"
            onPress={() => onRetry()}>
            Réessayer
          </Button>
        )}
      </Flex>
    </>
  );
}

export {ErrorMessage};
