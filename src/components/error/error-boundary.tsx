import {Button, Center, Text} from 'native-base';
import React from 'react';
import RNRestart from 'react-native-restart';

type State = {error: Error | null; errorInfo?: any};

export class ErrorBoundary extends React.Component {
  state: State;
  constructor(props: any) {
    super(props);
    this.state = {error: null, errorInfo: null};
  }

  static getDerivedStateFromError(error: Error): State {
    return {error};
  }

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
  }

  onRestartApp = () => {
    RNRestart.Restart();
  };

  render() {
    return this.state.error ? (
      <Center h="full">
        <Text
          color="red.700"
          fontSize="lg"
          fontWeight="bold"
          textAlign="center"
          textTransform="uppercase">
          Oops, Quelque chose s'est mal passé
        </Text>
        <Button
          size="sm"
          variant="subtle"
          colorScheme="red"
          marginTop="3"
          onPress={() => this.onRestartApp()}>
          Retour à l'acceuil
        </Button>
      </Center>
    ) : (
      this.props.children
    );
  }
}
