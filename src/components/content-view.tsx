/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useContext, useLayoutEffect, useState} from 'react';
import {Box, ScrollView, StatusBar} from 'native-base';
import {ContentViewProps} from '../@core/props/content-view';
import {SearchBar} from './search-bar';
import {PlayerContext} from '../@core/contexts/player.context';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';

function ContentView({
  children,
  backgroundColor,
  barStyle,
  barColor,
  showSearchBar,
  searchBarConfig,
  contentPadding = 4,
}: ContentViewProps) {
  const tabBarHeight = useBottomTabBarHeight();
  const [bottomPadding, setBottomPadding] = useState<number>(0);
  const {currentTrack} = useContext(PlayerContext);

  useLayoutEffect(() => {
    setBottomPadding(tabBarHeight / 4);
  }, [tabBarHeight, currentTrack]);

  return (
    <>
      <Box
        safeAreaTop={2}
        height="full"
        backgroundColor={backgroundColor || 'white'}
        paddingX={contentPadding}>
        <StatusBar
          backgroundColor={barColor || 'white'}
          barStyle={barStyle || 'dark-content'}
        />
        {showSearchBar && (
          <SearchBar
            marginBottom={searchBarConfig?.marginBottom}
            padding={searchBarConfig?.padding}
          />
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            flexGrow: 1,
            paddingBottom: bottomPadding,
          }}>
          <Box>{children}</Box>
        </ScrollView>
      </Box>
    </>
  );
}

export {ContentView};
