/* eslint-disable react-hooks/exhaustive-deps */
import React, {useRef, useEffect} from 'react';
import {Animated, Easing} from 'react-native';
import {useRoute} from '@react-navigation/native';

function FadeInView({children}: any) {
  const {name} = useRoute();
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      useNativeDriver: true,
      toValue: 1,
      easing: Easing.ease,
      duration: 350,
    }).start();
  }, [fadeAnim, name]);

  return (
    <Animated.View
      style={{
        opacity: fadeAnim,
        transform: [
          {
            scale: fadeAnim.interpolate({
              inputRange: [0, 1],
              outputRange: [0.7, 1],
            }),
          },
          {
            translateY: fadeAnim.interpolate({
              inputRange: [0, 1],
              outputRange: [30, 0],
            }),
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
}

export {FadeInView};
