import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Player} from '../views/player';
import {Tabs} from '../navigators/tabs';
import {PlayerProvider} from './player/player-provider';

const Stack = createNativeStackNavigator();

function AppWrapper() {
  return (
    <>
      <PlayerProvider>
        <NavigationContainer>
          <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen
              name="Tabs"
              component={Tabs}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Player"
              component={Player}
              options={{presentation: 'modal', animation: 'slide_from_bottom'}}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </PlayerProvider>
    </>
  );
}

export {AppWrapper};
