import React, {useContext} from 'react';
import {Flex, Icon, Input, Pressable} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {NavigationContext, useRoute} from '@react-navigation/native';
import {Colors} from '../@core/utils/colors';
import {SearchBarProps} from '../@core/props/search-bar-props';
import {useAppDispatch} from '../store/store';
import {setSearchText} from '../store/slices/emission.slice';

function SearchBar({padding = 0, marginBottom = 4}: SearchBarProps) {
  const dispatch = useAppDispatch();
  const route = useRoute();
  const navigation = useContext(NavigationContext);

  const onPressBack = () => {
    if (route.name !== 'Acceuil') {
      navigation?.canGoBack()
        ? navigation?.goBack()
        : navigation?.navigate('Tabs');
    }
  };

  const onInputFocus = () => {
    if (route.name !== 'ReplayList') {
      navigation?.navigate('Tabs', {screen: 'Replay'});
    }
  };

  const onInput = (value: string) => {
    if (value === '') {
      dispatch(setSearchText(''));
      return;
    }
    if (value.length > 3) {
      dispatch(setSearchText(value));
      return;
    }
  };

  return (
    <>
      <Flex
        align="center"
        justify="center"
        direction="row"
        paddingX={padding}
        marginBottom={marginBottom}>
        <Input
          onFocus={onInputFocus}
          onSubmitEditing={e => onInput(e.nativeEvent.text)}
          variant="underlined"
          placeholder="Recherche"
          placeholderTextColor="red.700"
          borderColor="red.700"
          borderBottomWidth="1"
          paddingBottom="3"
          InputLeftElement={
            route.name !== 'Acceuil' ? (
              <Pressable onPress={onPressBack} paddingLeft="2" paddingY="2">
                <Icon
                  as={Ionicons}
                  name="arrow-back-outline"
                  size={7}
                  mr="2"
                  color="red.700"
                />
              </Pressable>
            ) : (
              <></>
            )
          }
          InputRightElement={
            <Pressable onPress={onInputFocus} paddingLeft="2" paddingY="2">
              <Icon
                as={Ionicons}
                name="search"
                size={7}
                mr="2"
                color="red.700"
              />
            </Pressable>
          }
          _focus={{borderBottomColor: Colors.red400}}
        />
      </Flex>
    </>
  );
}

export {SearchBar};
