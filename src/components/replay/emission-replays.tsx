/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {Box, FlatList, Text, VStack} from 'native-base';
import {ReplayItem} from './replay-item';
import {useAppDispatch, useAppSelector} from '../../store/store';
import {selectEmissionState} from '../../store/slices/emission.slice';
import {fetchReplaysByEmissionID} from '../../store/middlewares/emission.thunk';
import {LoadingSpinner} from '../loading-spinner';
import {ErrorMessage} from '../error/error-message';

function EmissionReplays() {
  const dispatch = useAppDispatch();
  const {
    currentEmission,
    currentEmissionReplays,
    currentEmissionReplaysLoading,
    currentEmissionReplaysLoadingError,
  } = useAppSelector(selectEmissionState);

  const replayItem = ({item, index}: any) => {
    return (
      <ReplayItem
        replay={item}
        replayIndex={index}
        replayPlaylist={currentEmissionReplays}
      />
    );
  };

  useEffect(() => {
    if (currentEmission?.id) {
      dispatch(fetchReplaysByEmissionID(currentEmission?.id));
    }
  }, [currentEmission]);

  const onRetry = () => {
    if (currentEmission?.id) {
      dispatch(fetchReplaysByEmissionID(currentEmission?.id));
    }
  };

  return (
    <>
      {currentEmissionReplays?.length ? (
        <VStack>
          <Box marginBottom="5">
            <Text
              color="darkBlue.700"
              fontSize="2xl"
              fontWeight="extrabold"
              textAlign="left"
              textTransform="capitalize">
              Replays
            </Text>
          </Box>
          <FlatList
            data={currentEmissionReplays}
            renderItem={replayItem}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.id}
          />
        </VStack>
      ) : null}
      {currentEmissionReplaysLoading && <LoadingSpinner />}
      {currentEmissionReplaysLoadingError && (
        <ErrorMessage
          message={currentEmissionReplaysLoadingError}
          onRetry={onRetry}
        />
      )}
    </>
  );
}

export {EmissionReplays};
