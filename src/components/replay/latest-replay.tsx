/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Box, FlatList, Text, VStack} from 'native-base';
import {ReplayItem} from './replay-item';
import {useAppSelector} from '../../store/store';
import {selectEmissionState} from '../../store/slices/emission.slice';

function LatestReplay() {
  const {latestReplays} = useAppSelector(selectEmissionState);
  const replayItem = ({item, index}: any) => {
    return (
      <ReplayItem
        replay={item}
        replayIndex={index}
        replayPlaylist={latestReplays}
      />
    );
  };

  return (
    <>
      {latestReplays?.length ? (
        <VStack>
          <Box marginBottom="5">
            <Text
              color="darkBlue.700"
              fontSize="26px"
              fontWeight="bold"
              textAlign="left"
              textTransform="capitalize">
              Ajouts Récents
            </Text>
          </Box>
          <FlatList
            data={latestReplays}
            renderItem={replayItem}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.id}
            scrollToOverflowEnabled={false}
          />
        </VStack>
      ) : null}
    </>
  );
}

export {LatestReplay};
