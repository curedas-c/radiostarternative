import React, {useContext} from 'react';
import {Flex, Text, Image, Pressable, Icon as NIcon, VStack} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {PlayerContext} from '../../@core/contexts/player.context';
import {setTrackList} from '../../@core/services/player.service';
import {getTrackFromReplay} from '../../@core/utils/track-transform';
import {Images} from '../../assets/resource';
import {ReplayItemProps} from '../../@core/props/replay-item';

function ReplayItem({replay, replayIndex, replayPlaylist}: ReplayItemProps) {
  const {title, emissionTitle, logoUrl} = replay;
  const {currentTrack} = useContext(PlayerContext);

  const onClickStart = () => {
    if (currentTrack?.title === title) {
      return;
    }
    if (replayPlaylist) {
      setTrackList(getTrackFromReplay(replayPlaylist), replayIndex);
    }
  };

  return (
    <>
      <Pressable onPress={onClickStart}>
        <Flex
          justify="flex-start"
          direction="row"
          align="center"
          marginBottom="4">
          <Image
            source={{
              uri: logoUrl || Images.emissionLogo,
            }}
            fallbackSource={{
              uri: Images.emissionLogo,
            }}
            alt="Logo"
            size={12}
            resizeMode={'contain'}
            borderRadius="md"
          />
          <VStack marginLeft="4" marginRight="2" flexShrink="1">
            <Text
              numberOfLines={1}
              flexShrink="1"
              color="black"
              fontSize="14px"
              fontWeight="bold"
              textAlign="left"
              textTransform="capitalize">
              {title}
            </Text>
            <Text
              numberOfLines={1}
              flexShrink="1"
              color="darkBlue.100"
              fontSize="14px"
              textAlign="left"
              fontWeight="semibold"
              textTransform="capitalize">
              {emissionTitle}
            </Text>
          </VStack>
          <NIcon
            as={Icon}
            name="caret-forward"
            size="sm"
            color="darkBlue.700"
            marginLeft="auto"
          />
        </Flex>
      </Pressable>
    </>
  );
}

export {ReplayItem};
