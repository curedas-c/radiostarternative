import React from 'react';
import {AspectRatio, Flex, Image, Text, VStack, Pressable} from 'native-base';
import {Images} from '../assets/resource';
import {ContentView} from './content-view';
import {
  FACEBOOK_URL,
  MALIBA_MAIL,
  MALIBA_PHONE1,
  MALIBA_PHONE2,
  MALIBA_PHONE3,
  MALIBA_PHONE4,
  TWITTER_URL,
  YOUTUBE_URL,
} from '../@core/utils/constants';
import {
  PhoneCallIcon,
  MailIcon,
  FacebookIcon,
  YoutubeIcon,
  TwitterIcon,
} from '../@core/utils/icons';
import {onOpenLink} from '../@core/utils/link';

function Contacts() {
  return (
    <>
      <ContentView
        showSearchBar={true}
        contentPadding={0}
        searchBarConfig={{padding: 4}}>
        <Flex height="full" align="center" justify="space-between">
          <Flex width="full" align="center" marginTop="2">
            <Image
              source={{
                uri: Images.emissionLogo,
              }}
              alt="Logo"
              size="xl"
              resizeMode={'contain'}
            />
            <AspectRatio ratio={16 / 8} width="full">
              <Image
                source={{
                  uri: Images.map,
                }}
                alt="Map"
                width="full"
                height="full"
                resizeMode={'cover'}
              />
            </AspectRatio>
          </Flex>
          <VStack width="full" paddingX="5">
            <Text
              color="darkBlue.700"
              fontSize="lg"
              fontWeight="extrabold"
              textAlign="center"
              marginBottom="3">
              Gardons contact
            </Text>
            <Flex direction="row" align="center">
              <PhoneCallIcon size="md" marginRight="3" />
              <Flex>
                <Text
                  color="black"
                  fontSize="md"
                  textAlign="left"
                  marginBottom="1"
                  onPress={() => onOpenLink(`tel:${MALIBA_PHONE1}`)}>
                  {MALIBA_PHONE1}
                </Text>
                <Text
                  color="black"
                  fontSize="md"
                  textAlign="left"
                  marginBottom="1"
                  onPress={() => onOpenLink(`tel:${MALIBA_PHONE2}`)}>
                  {MALIBA_PHONE2}
                </Text>
                <Text
                  color="black"
                  fontSize="md"
                  textAlign="left"
                  marginBottom="1"
                  onPress={() => onOpenLink(`tel:${MALIBA_PHONE3}`)}>
                  {MALIBA_PHONE3}
                </Text>
                <Text
                  color="black"
                  fontSize="md"
                  textAlign="left"
                  onPress={() => onOpenLink(`tel:${MALIBA_PHONE4}`)}>
                  {MALIBA_PHONE4}
                </Text>
              </Flex>
            </Flex>
            <Pressable
              display="flex"
              flexDirection="row"
              alignItems="center"
              marginTop="3"
              marginBottom="4"
              paddingLeft="1"
              onPress={() => onOpenLink(`mailto:${MALIBA_MAIL}`)}>
              <MailIcon size="md" marginRight="3" />
              <Text
                numberOfLines={1}
                color="black"
                fontSize="md"
                textAlign="left">
                {MALIBA_MAIL}
              </Text>
            </Pressable>
          </VStack>
          <VStack width="full" paddingX="5" paddingBottom="2">
            <Text
              color="darkBlue.700"
              fontSize="lg"
              fontWeight="extrabold"
              textAlign="center"
              marginBottom="3">
              Suivez-nous
            </Text>
            <Flex direction="row" justify="space-around" align="center">
              <Pressable onPress={() => onOpenLink(FACEBOOK_URL)}>
                <FacebookIcon size="sm" />
              </Pressable>
              <Pressable onPress={() => onOpenLink(YOUTUBE_URL)}>
                <YoutubeIcon size="sm" />
              </Pressable>
              <Pressable onPress={() => onOpenLink(TWITTER_URL)}>
                <TwitterIcon size="sm" />
              </Pressable>
            </Flex>
          </VStack>
        </Flex>
      </ContentView>
    </>
  );
}

export {Contacts};
