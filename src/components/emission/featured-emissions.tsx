/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Box, Text, VStack, ScrollView} from 'native-base';
import {useAppSelector} from '../../store/store';
import {selectEmissionState} from '../../store/slices/emission.slice';
import {EmissionCard} from './emission-card';

function FeaturedEmissions() {
  const {featuredEmission, featuredEmissionLoading} =
    useAppSelector(selectEmissionState);

  const cards = featuredEmission?.map((emission, key) => {
    return (
      <Box width={175} key={key}>
        <EmissionCard emission={emission} />
      </Box>
    );
  });

  return (
    <>
      {!featuredEmissionLoading && featuredEmission?.length && (
        <VStack>
          <Box paddingBottom="4">
            <Text
              color="darkBlue.700"
              fontSize="26px"
              fontWeight="bold"
              textAlign="left"
              textTransform="capitalize">
              Emissions Vedettes
            </Text>
          </Box>
          <Box>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                display: 'flex',
                flexDirection: 'row',
              }}>
              {cards}
            </ScrollView>
          </Box>
        </VStack>
      )}
    </>
  );
}

export {FeaturedEmissions};
