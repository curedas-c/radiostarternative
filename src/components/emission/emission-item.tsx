import React, {useContext} from 'react';
import {Flex, Text, Image, Pressable, Icon as NIcon, VStack} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {EmissionCardProps} from '../../@core/props/emission-card';
import {Images} from '../../assets/resource';
import {useAppDispatch, useAppSelector} from '../../store/store';
import {
  selectEmissionState,
  setCurrentEmission,
} from '../../store/slices/emission.slice';
import {NavigationContext} from '@react-navigation/native';

function EmissionItem({emission}: EmissionCardProps) {
  const {title, animators, logoUrl} = emission;
  const navigation = useContext(NavigationContext);
  const dispatch = useAppDispatch();
  const {currentEmission} = useAppSelector(selectEmissionState);

  const onClick = () => {
    if (currentEmission?.id !== emission.id) {
      dispatch(setCurrentEmission(emission));
    }
    navigation?.navigate('Replay', {screen: 'EmissionDetails'});
  };

  return (
    <>
      <Pressable onPress={onClick}>
        <Flex
          justify="flex-start"
          direction="row"
          align="center"
          marginBottom="4">
          <Image
            source={{
              uri: logoUrl || Images.emissionLogo,
            }}
            fallbackSource={{
              uri: Images.emissionLogo,
            }}
            alt="Logo"
            size={12}
            resizeMode={'contain'}
            borderRadius="md"
          />
          <VStack marginLeft="4" marginRight="2" flexShrink="1">
            <Text
              color="darkBlue.700"
              flexShrink="1"
              numberOfLines={1}
              fontSize="18px"
              fontWeight="bold"
              textAlign="left"
              textTransform="capitalize">
              {title}
            </Text>
            {animators ? (
              <Text
                color="darkBlue.100"
                numberOfLines={1}
                flexShrink="1"
                fontSize="15px"
                textTransform="capitalize"
                textAlign="left">
                {animators}
              </Text>
            ) : null}
          </VStack>
          <NIcon
            as={Icon}
            name="chevron-forward"
            size="sm"
            color="darkBlue.700"
            marginLeft="auto"
          />
        </Flex>
      </Pressable>
    </>
  );
}

export {EmissionItem};
