import React, {useContext} from 'react';
import {Flex, Text, Image, Pressable, AspectRatio} from 'native-base';
import {EmissionCardProps} from '../../@core/props/emission-card';
import {Images} from '../../assets/resource';
import {useAppDispatch, useAppSelector} from '../../store/store';
import {
  selectEmissionState,
  setCurrentEmission,
} from '../../store/slices/emission.slice';
import {NavigationContext} from '@react-navigation/native';

function EmissionCard({
  emission,
  marginRight = 6,
  showTag = true,
}: EmissionCardProps) {
  const {title, rubric, animators, logoUrl} = emission;
  const navigation = useContext(NavigationContext);
  const dispatch = useAppDispatch();
  const {currentEmission} = useAppSelector(selectEmissionState);

  const onClick = () => {
    if (currentEmission?.id !== emission.id) {
      dispatch(setCurrentEmission(emission));
    }
    navigation?.navigate('Replay', {screen: 'EmissionDetails'});
  };

  return (
    <>
      <Pressable onPress={onClick}>
        <Flex align="flex-start" justify="center" marginRight={marginRight}>
          <AspectRatio ratio={1 / 1} width="full" marginBottom="3">
            <Image
              source={{
                uri: logoUrl || Images.emissionLogo,
              }}
              fallbackSource={{
                uri: Images.emissionLogo,
              }}
              alt="Logo"
              width="full"
              height="full"
              rounded="xl"
              resizeMode={'cover'}
            />
          </AspectRatio>
          <Text
            color="darkBlue.700"
            numberOfLines={2}
            fontSize="18px"
            fontWeight="bold"
            textAlign="left"
            textTransform="capitalize">
            {title}
          </Text>
          {showTag && rubric && (
            <Text
              color="red.700"
              numberOfLines={1}
              fontSize="15px"
              textTransform="capitalize"
              textAlign="left">
              {rubric}
            </Text>
          )}
          {!showTag && animators && (
            <Text
              color="red.700"
              numberOfLines={1}
              fontSize="md"
              textTransform="capitalize"
              textAlign="left">
              Par {animators}
            </Text>
          )}
        </Flex>
      </Pressable>
    </>
  );
}

export {EmissionCard};
