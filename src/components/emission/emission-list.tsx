/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Box, Text, FlatList} from 'native-base';
import {EmissionItem} from './emission-item';
import {LoadingSpinner} from '../loading-spinner';
import {useAppDispatch, useAppSelector} from '../../store/store';
import {selectEmissionState} from '../../store/slices/emission.slice';
import {RefreshControl} from 'react-native';
import {
  fetchEmissions,
  fetchEmissionsByTag,
  fetchEmissionsByTitle,
} from '../../store/middlewares/emission.thunk';
import {wait} from '../../@core/utils/util';

function EmissionList() {
  const dispatch = useAppDispatch();
  const {
    rubricsLoading,
    emissionsLoading,
    emissions,
    selectedRubric,
    currentSearchText,
  } = useAppSelector(selectEmissionState);
  const [refreshing, setRefreshing] = useState(false);
  const emissionItem = ({item}: any) => {
    return <EmissionItem emission={item} />;
  };

  const onRefresh = () => {
    setRefreshing(true);
    wait(100).then(() => setRefreshing(false));

    if (currentSearchText !== '') {
      dispatch(fetchEmissionsByTitle(currentSearchText));
    } else if (!selectedRubric) {
      dispatch(fetchEmissions());
    } else {
      dispatch(fetchEmissionsByTag(selectedRubric.id));
    }
  };

  return (
    <>
      {emissions?.length && !emissionsLoading ? (
        <FlatList
          width="full"
          height="full"
          marginTop="10"
          data={emissions}
          renderItem={emissionItem}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              colors={['#023759']}
              onRefresh={onRefresh}
            />
          }
        />
      ) : null}
      {emissions?.length === 0 && !emissionsLoading ? (
        <Text
          marginTop="10"
          color="darkBlue.700"
          fontSize="xl"
          fontWeight="bold"
          textAlign="center">
          Aucun élément à afficher ou une erreur s'est produite.
        </Text>
      ) : null}
      {emissionsLoading && !rubricsLoading && (
        <Box marginTop="10">
          <LoadingSpinner />
        </Box>
      )}
    </>
  );
}

export {EmissionList};
