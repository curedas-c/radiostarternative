/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import {extendTheme, NativeBaseProvider} from 'native-base';
import {Provider} from 'react-redux';
import {store} from './src/store/slices/root.slice';
import {AppWrapper} from './src/components/app-wrapper';
import {ErrorBoundary} from './src/components/error/error-boundary';

const nativeBaseConfig = {
  dependencies: {
    'linear-gradient': require('react-native-linear-gradient').default,
  },
};

const nativeBaseTheme = extendTheme({
  colors: {
    darkBlue: {
      100: '#02375980',
      700: '#023759',
    },
    red: {
      400: '#EC473F40',
      700: '#EC473F',
    },
  },
  fontConfig: {
    Inter: {
      100: {
        normal: 'Inter-Light',
      },
      200: {
        normal: 'Inter-Light',
      },
      300: {
        normal: 'Inter-Light',
      },
      400: {
        normal: 'Inter-Regular',
      },
      500: {
        normal: 'Inter-Medium',
      },
      600: {
        normal: 'Inter-SemiBold',
      },
      700: {
        normal: 'Inter-Bold',
      },
      800: {
        normal: 'Inter-ExtraBold',
      },
      900: {
        normal: 'Inter-ExtraBold',
      },
    },
  },
  fonts: {
    heading: 'Inter',
    body: 'Inter',
    mono: 'Inter',
  },
});

export default function App() {
  return (
    <Provider store={store}>
      <NativeBaseProvider config={nativeBaseConfig} theme={nativeBaseTheme}>
        <ErrorBoundary>
          <AppWrapper />
        </ErrorBoundary>
      </NativeBaseProvider>
    </Provider>
  );
}
