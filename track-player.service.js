import TrackPlayer, {Event} from 'react-native-track-player';

module.exports = async function setup() {
  TrackPlayer.addEventListener(Event.RemotePause, () => {
    TrackPlayer.pause().catch(err => {
      console.warn('from track service js', err);
    });
  });

  TrackPlayer.addEventListener(Event.RemotePlay, () => {
    TrackPlayer.play().catch(err => {
      console.warn('from track service js', err);
    });
  });

  TrackPlayer.addEventListener(Event.RemoteNext, () => {
    TrackPlayer.skipToNext().catch(err => {
      console.warn('from track service js', err);
    });
  });

  TrackPlayer.addEventListener(Event.RemotePrevious, () => {
    TrackPlayer.skipToPrevious().catch(err => {
      console.warn('from track service js', err);
    });
  });

  TrackPlayer.addEventListener(Event.RemoteStop, () => {
    TrackPlayer.stop().catch(err => {
      console.warn('from track service js', err);
    });
  });
};
